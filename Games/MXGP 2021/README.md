# Tweaks

You need to install `vcrun2019`, for example:
```
DISPLAY=:0.0 batocera-wine windows tricks  /userdata/saves/windows/proton/MXGP\ 2021\ -\ The\ Official\ Motocross\ Videogame.pc.wine vcrun2019
```
