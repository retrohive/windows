set dotenv-load

build ARG:
	docker run -v "${PWD}/{{ARG}}:/code" retrohive-builder

clean:
	find Games -not -name '*.md' -not -name 'PKGBUILD' -not -wholename './.git*' -not -name 'Justfile' -not -name 'description.xml' -type f -print0 | xargs -0 -n1 rm -v

deploy:
  rsync --rsync-path="sudo rsync" -avzr --inplace --progress Games/*/*.zst "$SSH_LOGIN@$SSH_HOST:$SSH_PATH"
  ssh $SSH_LOGIN@$SSH_HOST "sudo ${SCRIPT}"
